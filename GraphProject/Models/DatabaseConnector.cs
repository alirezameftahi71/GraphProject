﻿using System.Data;
using System.Data.SqlClient;

namespace GraphProject.Models
{
    public class DatabaseConnector
    {
        private static string dbName = "armGraph";
        private static string tblName = "dbo.graphs";
        private static string clnmName = "obj";
        private static string urlConnection = "Data Source=.;Initial Catalog=" + dbName + ";Integrated Security=True";
        public static void saveData(byte[] inputData)
        {

            SqlConnection conn = new SqlConnection(urlConnection);
            conn.Open();
            using (SqlCommand cmd = new SqlCommand("INSERT INTO " + tblName + "(" + clnmName + ") VALUES (@binaryValue)", conn))
            {
                cmd.Parameters.Add("@binaryValue", SqlDbType.VarBinary, -1).Value = inputData;
                cmd.ExecuteNonQuery();
            }
            conn.Close();
        }

        public static Graph loadData()
        {
            Graph graph = null;
            SqlConnection conn = new SqlConnection(urlConnection);
            conn.Open();
            using (SqlCommand cmd = new SqlCommand("SELECT TOP 1 " + clnmName + " FROM " + tblName + " ORDER BY id DESC", conn))
            {
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    byte[] retrivedBytes = (byte[])rdr[clnmName];
                    graph = new Graph();
                    graph = Serializer.Deserialize(retrivedBytes);
                }
            }
            conn.Close();

            return graph;
        }
    }
}