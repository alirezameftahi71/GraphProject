﻿using GraphProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GraphProject.Controllers
{
    public class DefaultController : Controller
    {
        // GET: Default
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public bool Store(string jsonObj)
        {
            Graph graph = new Graph();
            graph.setJsonData(jsonObj);
            DatabaseConnector.saveData(Serializer.Serialize(graph));
            return true;
        }

        public String Restore()
        {
            Serializer serializer = new Serializer();
            Graph graph = DatabaseConnector.loadData();
            try
            {
                return graph.getJsonData();
            }
            catch (NullReferenceException e)
            {
                return null;
            }
        }
    }
}